[![Sauce Test Status](https://saucelabs.com/buildstatus/browser_tests1)](https://saucelabs.com/u/browser_tests2)

[![Sauce Test Status](https://saucelabs.com/browser-matrix/browser_tests1.svg)](https://saucelabs.com/u/browser_tests2)

# tutorial

The best project ever.

## Getting Started

### Run Tests on one browser

```
grunt test:sauce:chrome
grunt test:sauce:firefox
grunt test:sauce:explorer
```

### Run Parallel Tests

```
grunt test:sauce:parallel
```

## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [Grunt](http://gruntjs.com/).

_Also, please don't edit files in the "dist" subdirectory as they are generated via Grunt. You'll find source code in the "lib" subdirectory!_

## Release History
_(Nothing yet)_

## License
Copyright (c) 2014 Jonas Colmsjö  
Licensed under the MIT license.
